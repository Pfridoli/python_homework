# README #

### What is this program? ###

Basically a web scraping program, which finds most common used words on the website.

Also makes csv file which contains the result.

### How do I get set up? ###
	
0.  This program needs BeautifulSoup.

    Use this command to install BeautifulSoup: **pip install beautifulsoup4**

    If that doesn't help, check this website for help: [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)

1.  This program can be started with terminal or IDE.

    To start this program, write this command to the terminal: **python main.py**

    For Python 3, use this command: **python3 main.py**

    For Python 2, use this command: **python2 main.py**

2.  Next, it's going to ask website address(full name only)

3.  Next, it's going to ask, if you want csv file (contains all counted words).

4.  Finally it's going to show the result.

    If you asked for the csv file, it's in the same place, where the program is.

### Unittest ###

If you want to do unitest, use this command with terminal: **python -m unittest -v main**

For Python 3, use this command: **python3 -m unittest -v main.py**

For Python 2, use this command: **python -m unittest -v main**

### Who do I talk to? ###

Peeter Fridolin 
