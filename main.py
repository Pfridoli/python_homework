from collections import Counter
from bs4 import BeautifulSoup #BeautifulSoup must be installed
from bs4.element import Comment
from six.moves import input
import requests
import re
import csv
import unittest

class ConnectionTestCase(unittest.TestCase):
    def setUp(self):
        self.words = ['\n', 'Test\n', 'Test$^%#@', '1256', 'H', '   ', 'TeST55']
        self.words_result = 'test test test'
        self.count_words = 'does does this does this work'
        self.count_result = [('does', 3), ('this', 2), ('work', 1)]
        self.html = '<html><head><title>test</title></head><body id="foo" onload="whatever"><p class="whatever">junk</p><div style="background: yellow;" id="foo" class="blah">blah</div></body></html>'
        self.html_result = 'junk blah'

    def test_text_corrector(self):
        test_result1 = text_corrector(self.words)
        self.assertEqual(test_result1, self.words_result)

    def test_word_counter(self):
        test_result2 = word_counter(self.count_words)
        self.assertEqual(test_result2, self.count_result)

    def test_tag_visible(self):
        soup_test = BeautifulSoup(self.html, 'html.parser')
        texts_test = soup_test.findAll(text=True)
        words_test = filter(tag_visible, texts_test)
        test_result3 = text_corrector(words_test)
        self.assertEqual(test_result3, self.html_result)


#Removes html elements
def tag_visible(element):
    if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
        return False
    if isinstance(element, Comment):
        return False
    return True

#Removes unnecessary symbols from text
def text_corrector(text):
    text = u" ".join(t.strip() for t in text) #removes html letters
    text = re.sub(r"[^\w']+" , " ", text) #removes random symbols
    text = re.sub("\d+", " ", text) #removes numbers
    text = ' '.join( [w for w in text.split() if len(w)>1] ) #removes single letters
    text = text.lower() #makes words lowercase
    return text

#Starts counting words from text
def word_counter(text):
    counted = Counter(text.split()).most_common()
    return counted

#Makes csv file and puts all of the data into the file
def csv_maker(counted, text):
    print("Do you want csv file (yes or no)")
    while(True):
        answer = input()
        if answer.lower() in ["yes", "y"]:
            with open('WordCounted.csv','w') as csvfile:
                fieldnameChar = ['Total of characters are: ', len(text)]
                fieldnameWord = ['Total of words are: ', len(text.split())]
                fieldnameCounted = ['Word','Count']
                writer = csv.writer(csvfile)
                writer.writerow(fieldnameWord)
                writer.writerow(fieldnameChar)
                writer.writerow("")
                writer.writerow(fieldnameCounted)
                for key, value in counted:
                    writer.writerow([key,value])
                break
        elif answer.lower() in ["no", "n"]:
            break
        else:
            print("Please enter YES or NO only")

#Starts taking text from the website
def text_from_html(body):
    soup = BeautifulSoup(body.content, 'html.parser')
    texts = soup.findAll(text=True)
    visible_texts = filter(tag_visible, texts)
    text = text_corrector(visible_texts)
    counted = word_counter(text)
    csv_maker(counted, text)
    return counted, text

#Asking user for website address
def userinput():
    print("Enter website address (must be like https://www.google.com)")
    while(True):
        try:
            url = input()
            html = requests.get(url)
            break
        except Exception:
            print ("Entered website address is wrong. Try again.")
    return html

#Main method
def main():

    html = userinput()
    counted_words, text = text_from_html(html)
    print(counted_words)
    print("Total of words are: {0}".format(len(text.split())))
    print("Total of characters are: {0}".format(len(text)))

#Starts the script
if __name__ == "__main__":
    main()
